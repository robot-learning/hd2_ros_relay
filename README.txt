This ROS package allows you to read data sent from a Quaner HD2 and has an example for using that data to control a Baxter robot. 
From the HD2 computer with QUARC and Simulink, use the Stream Client block just like the HD2_Controller_NY_Teleop.mdl does 
(replace NY with PY if using right-handed HD2), where you specify the IP address of the computer you are trying to send data to,
the port the receiving computer will be listening on, and whether you are using TCP/IP or UDP (this package is using UDP).

In this package, change the hd2_relay.launch file and the hd2_relay_node.cpp file to reflect the IP address of the HD2 computer.

The hd2_relay_node.cpp reads the HD2 data from the port and publishes it onto the rostopic /hd2/robot_state.
You can change rate at which the data is published from this file like with other ROS publishers.

The HD2State.msg file specifies what types of messages are being published onto the rostopic /hd2/robot_state.

hd2_relay_test.py is a test script that produces random data for hd2_relay_node.cpp to read, just change the IP
addresses in the hd2_relay.launch file and the hd2_relay_node.cpp file to 127.0.0.1 and the port to 18000

Baxter_slave_IK is an example subscriber node where it reads HD2 pose data from the rostopic /hd2/robot_state and
uses baxter_pykdl so that you can use the position of the HD2 to control the position of one of the Baxter's arms
(there is no IK going on in the Baxter control now despite the name of the script). This script will currently not
do anything if there is nothing for hd2_relay_node to read.

The simplest place to start is seeing if you can ping the computer you are trying to communicate with!

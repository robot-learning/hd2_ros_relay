// STL
#include <string>

// ROS Stuff
#include <ros/ros.h>
#include <hd2_ros_relay/HD2State.h>
// TCP/IP Comm stuff
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>

typedef struct
{
  //The 8 double values to be sent represent the following signals:



  //If you want to read the HD2's positions, use this:

  double pos_x; // Position in the x-direction (m)
  double pos_y; // Position in the y-direction (m)
  double pos_z; // Position in the z-direction (m)
  double quat_x; // x-component of quaternion (for orientation)
  double quat_y; // y-component of quaternion (for orientation)
  double quat_z; // z-component of quaternion (for orientation)
    //If you want to read the HD2's velocities, use this:
  
  double linear_x; // Linear speed command along the x-axis (m/s)
  double linear_y; // Linear speed command along the y-axis (m/s)
  double linear_z; // Linear speed command along the z-axis (m/s)
  double angular_x; // Angular rate command about the x-axis (rad/s)
  double angular_y; // Angular rate command about the y-axis (rad/s)
  double angular_z; // Angular rate command about the z-axis (rad/s)
 


  double switch_rate; // Handle switch (button) rate command (rad/s)
  double pedal_enable; // Foot pedal (enable) on (1)/off (0)
} HD2MsgData;


/**
 * Receive HD^2 data over the bound socket
 *
 * @param udp_socket  Bound udp_socket
 * @param data        Data buffer to store packet into
 * @param remote_addr address of the HD^2 remote computer
 *
 * @return Signal length of msg received, 0 if no msg, or -1 if failed
 */
int receive_hd2_packet(int udp_socket, HD2MsgData* data, struct sockaddr_in* hd2_addr)
{
  if (udp_socket < 0)
  {
    return -1;
  }
  socklen_t sock_addr_size = sizeof(struct sockaddr_in);
  return recvfrom(udp_socket, (char *) data, sizeof(HD2MsgData), 0,
                  (struct sockaddr *) hd2_addr, &sock_addr_size);
}

/**
 * Starts up a node that binds to the requested port and sends data over ros when it receives messages
 *
 * @param argc number of arguments
 * @param argv command line arguments expects nothing currently
 * robot name
 *
 * @return 0 if succesfful, 1 otherwise
 */
int main(int argc,char* argv[])
{
  // Initialize ROS infrastructrues
  ros::init(argc,argv,"hd2_relay_node");
  ros::NodeHandle n;
  ros::Publisher hd2_state_publisher = n.advertise<hd2_ros_relay::HD2State>("/hd2/robot_state/", 1);
  //ros::Rate loop_rate(100); #loop_rate(100) would publish the data at 100 Hz if you use loop_rate.sleep() at the bottom of this file
  hd2_ros_relay::HD2State cur_state;

  // Setup information for the UDP or TCP port, reading parameters from the ros server
  ROS_INFO_STREAM(ros::Time::now());
  int hd2_port;
  int udp_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  n.param("hd2_port",hd2_port,18000);
  std::string hd2_ip_address;
  n.param<std::string>("hd2_ip_address", hd2_ip_address,"192.168.1.150");

  // Setup local and remote addresses
  struct sockaddr_in local_addr;
  struct sockaddr_in hd2_addr;
  memset(&local_addr, 0, sizeof(local_addr));
  memset(&hd2_addr, 0, sizeof(hd2_addr));
  local_addr.sin_family = AF_INET;
  local_addr.sin_addr.s_addr = INADDR_ANY;
  local_addr.sin_port = htons(hd2_port);
  // Setup remote address
  hd2_addr.sin_addr.s_addr = inet_addr(hd2_ip_address.c_str());
  hd2_addr.sin_family = AF_INET;
  hd2_addr.sin_port = htons(hd2_port);

  // Bind to local socket
  if (bind(udp_socket, (struct sockaddr *) &local_addr, sizeof(sockaddr_in)) < 0)
  {
    ROS_ERROR("binding port number %d failed!\n", hd2_port);
    if (udp_socket >= 0)
    {
      close(udp_socket);
    }
    return 1;
  }
  else
  {
    ROS_INFO_STREAM("Succesfully bound to port " << hd2_port);
  }

  int received = -1;
  HD2MsgData hd2_data;
  memset((void*)(& hd2_data),0x0,sizeof(hd2_data));
  // Spin until interrupted
  while(ros::ok())
  {
    // Receive data over port (blocking)
    received =  receive_hd2_packet(udp_socket, &hd2_data, &hd2_addr);

    // Parse packet information into ROS msg

    //If you want to read the HD2's velocities, use this:
    
    cur_state.velocity.linear.x = hd2_data.linear_x;
    cur_state.velocity.linear.y = hd2_data.linear_y;
    cur_state.velocity.linear.z = hd2_data.linear_z;
    cur_state.velocity.angular.x = hd2_data.angular_x;
    cur_state.velocity.angular.y = hd2_data.angular_y;
    cur_state.velocity.angular.z = hd2_data.angular_z;
  

    //If you want to read the HD2's positions, use this:
    cur_state.full_pose.position.x = hd2_data.pos_x;
    cur_state.full_pose.position.y = hd2_data.pos_y;
    cur_state.full_pose.position.z = hd2_data.pos_z;
    cur_state.full_pose.orientation.x = hd2_data.quat_x;
    cur_state.full_pose.orientation.y = hd2_data.quat_y;
    cur_state.full_pose.orientation.z = hd2_data.quat_z;

    //The HD2State.msg file should reflect whether you are reading pose data or velocity data from HD2

    cur_state.switch_rate = hd2_data.switch_rate;
    cur_state.pedal_enable = static_cast<int>(hd2_data.pedal_enable);

    cur_state.header.stamp=ros::Time::now();
    // Publish states over the stream
    hd2_state_publisher.publish(cur_state);
    ros::spinOnce();
    //loop_rate.sleep(); # Makes it so that data is published at loop_rate which is defined above
  }
  return 0;
}

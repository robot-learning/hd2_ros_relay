#!/usr/bin/env python

import rospy
#from std_msgs.msg import String
from hd2_ros_relay.msg import HD2State
import geometry_msgs.msg

from baxter_pykdl import baxter_kinematics
import baxter_interface
import numpy
import math
from transformations import euler_from_quaternion

#def callback(data):
    #rospy.loginfo(rospy.get_caller_id() + 'I heard %s', data.full_pose.position.x)

gripper_calibrated = False
switch_calibrated = False
switch_offset = 0

def callback(data):
    
    global gripper_calibrated 
    gripper = baxter_interface.Gripper('right')
    
    #Calibrate the gripper
    if gripper_calibrated == False:
    	gripper.calibrate()
    	gripper_calibrated = True
    
    #Read the switch position of the Quanser HD2
    hd2_switch_pos = data.switch_rate #It is the switch angle, not the rate. I need to change this.
    
    #Assume that the switch is not pressed the first time data is sent
    global switch_calibrated
    global switch_offset

    if switch_calibrated == False:
    	switch_offset = (0 - hd2_switch_pos)
    	switch_calibrated = True
    hd2_switch_pos = hd2_switch_pos + switch_offset
    #Map the range of the HD2 switch (0 ish to 0.53 ish) to the Baxter gripper (100 to 0)
    baxter_gripper_pos = 100 - hd2_switch_pos * (100/0.53)
    if baxter_gripper_pos > 100:
    	baxter_gripper_pos = 100
    if baxter_gripper_pos < 0:
    	baxter_gripper_pos = 0
    gripper.set_moving_force(1) #Limit force that gripper can exert
    gripper.command_position(baxter_gripper_pos, timeout = 0.01)

    #Use Jacobian Pseudoinverse from baxter_pykdl to control the Baxter
    kin = baxter_kinematics('right')
    limb = baxter_interface.Limb('right')
    fk = kin.forward_position_kinematics() #returns numpy array [x, y, z, rot_i, rot_j, rot_k, rot_w]
    #Elements of quaternion representation of the current orientation of the Baxter
    qx = fk[3]
    qy = fk[4]
    qz = fk[5]
    qw = fk[6]
    #Make it so that HD2 position matches the Baxter's workspace: (Coordinate systems of HD2 and Baxter are not aligned, make HD2's +y be Baxter's +x and the HD2's +x be the Baxter's -y
    quanser_pos = [data.full_pose.position.y + (0.60 - (0.1862)), -data.full_pose.position.x - (0.1194 + 0.255), data.full_pose.position.z + (0.043 - (-0.006498))]
    print quanser_pos
    #Desired XYZ (roll pitch yaw) Euler angles
    des_euler_angles = [-1*data.full_pose.orientation.y, -data.full_pose.orientation.x , data.full_pose.orientation.z]

    #Convert quaternion obtained from Baxter forward kinematics to roll pitch yaw Euler angles (it might be worth it to use tf in order to do this conversion: http://wiki.ros.org/tf/Tutorials)
    #Right now try this transformations.py: http://www.lfd.uci.edu/~gohlke/code/transformations.py.html
    y_squared = qy * qy
    t0 = -2 * (y_squared + qz * qz) + 1
    t1 = 2 * (qx * qy - qw * qz)
    t2 = -2 * (qx * qz + qw * qy)
    t3 = 2 * (qy * qz - qw * qx)
    t4 = -2 * (qx * qx + y_squared) + 1

    if t2 > 1:
    	t2 = 1

    if t2 < -1:
    	t2 = -1

    pitch1 = math.asin(t2);
    roll1 = math.atan2(t3, t4);
    #roll1 = roll1 + 2.52 # Offset to make it so that when arm is pointing down then roll is zero
    #if roll1 < 0:	#The roll flips signs (because of multiple solutions?) make it always positive (this probably only works for some limited portion of the workspace)
    	#roll1 = roll1 * -1
    yaw1 = math.atan2(t1, t0);
    
    roll, pitch, yaw = euler_from_quaternion([qw, qx, qy, qz], axes = 'sxyz')
    #Deal with discontinuity at pi
    if roll1 < 0:
    	roll1 = 2 * math.pi + roll1

    roll1 = roll1 - 3.14 #Offset to make it so that when arm is pointing down then roll is zero
    current_euler_angles = [roll1, pitch1, yaw1]
    #Estimate the desired angular velocity as being the difference between the desired Eulera angles and the current Euler angles
    angular_vel = [des_euler_angles[0] - current_euler_angles[0], des_euler_angles[1] - current_euler_angles[1], des_euler_angles[2] - current_euler_angles[2]]
    #angular_vel = [-0.0, -0.5, -0.0]
    
    #print des_euler_angles
    #print current_euler_angles
    #print angular_vel

    ref_pos = quanser_pos #Desired position
    current_pos = [fk[0], fk[1], fk[2]] #Current position
    error_vec = [ref_pos[0] - current_pos[0], ref_pos[1] - current_pos[1], ref_pos[2] - current_pos[2]]
    J_pseudo_inv = kin.jacobian_pseudo_inverse()
    K_lin = 2 #Translational gain
    K_rot = 1 #Rotational gain
    #Use the error position vector and scale it, use it as the translational velocity command:
    #Only command nonzero velocities when pedal is pressed
    hd2_pedal = data.pedal_enable # This is an int
    if hd2_pedal == 0:
    	X_dot = numpy.array([0,0,0,0,0,0]) 
    elif hd2_pedal == 1:
    	X_dot = numpy.array([error_vec[0]*K_lin, error_vec[1]*K_lin, error_vec[2]*K_lin, -angular_vel[0]*K_rot, angular_vel[1]*K_rot, angular_vel[2]*K_rot])
    else:
    	X_dot = numpy.array([0,0,0,0,0,0])
    #X_dot = numpy.array([error_vec[0]*K_lin, error_vec[1]*K_lin, error_vec[2]*K_lin, -angular_vel[0]*K_rot, angular_vel[1]*K_rot, angular_vel[2]*K_rot])
    Vel = J_pseudo_inv.dot(X_dot)
    theta_dot = [Vel[0,0], Vel[0,1], Vel[0,2],Vel[0,3], Vel[0,4], Vel[0,5], Vel[0,6]]
    joints = ["right_s0", "right_s1", "right_e0", "right_e1", "right_w0", "right_w1", "right_w2"]
    #Put theta_dot into a dictionary with joint labels
    theta_dot_dict = {}
    angle_iter = 0
    if theta_dot is not None:
    	for arg in joints:
    		theta_dot_dict[arg] = theta_dot[angle_iter]
        	angle_iter += 1
    
    #Velocity command to Baxter:
    limb.set_joint_velocities(theta_dot_dict)
    #print current_pos
    #print quanser_pos
    #print data




def listener():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('listener', anonymous=True)
    
    #queue_size of 1 makes it so that it only uses the last set of data it received. If there is a larger queue
    #or no queue_size argument then there will be a delay between when HD2 data is published to the topic
    #and when this node reads that data
    rospy.Subscriber('/hd2/robot_state/', HD2State, callback, queue_size = 1)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    listener()


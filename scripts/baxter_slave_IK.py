#!/usr/bin/env python

import rospy
#from std_msgs.msg import String
from hd2_ros_relay.msg import HD2State
import geometry_msgs.msg

from baxter_pykdl import baxter_kinematics
import baxter_interface
import numpy

#def callback(data):
    #rospy.loginfo(rospy.get_caller_id() + 'I heard %s', data.full_pose.position.x)


def callback(data):
    

    #Use Jacobian Pseudoinverse from baxter_pykdl to control the Baxter
    kin = baxter_kinematics('right')
    limb = baxter_interface.Limb('right')
    fk = kin.forward_position_kinematics() #returns numpy array [x, y, z, rot_i, rot_j, rot_k, rot_w] 
    #Make it so that HD2 position matches the Baxter's workspace:
    quanser_pos = [data.full_pose.position.x + (0.75 - (-0.10967569019748635)), data.full_pose.position.y + (-0.255 - (-0.1885665185510303)), data.full_pose.position.z + (0.043 - (-0.013591962847012168))]
    ref_pos = quanser_pos #Desired position
    current_pos = [fk[0], fk[1], fk[2]] #Current position
    error_vec = [ref_pos[0] - current_pos[0], ref_pos[1] - current_pos[1], ref_pos[2] - current_pos[2]]
    J_pseudo_inv = kin.jacobian_pseudo_inverse()
    K = 2
    #Use the error position vector and scale it, use it as the translational velocity command. No angular velocity for now:
    X_dot = numpy.array([error_vec[0]*K, error_vec[1]*K, error_vec[2]*K, 0, 0, 0]) 
    Vel = J_pseudo_inv.dot(X_dot)
    theta_dot = [Vel[0,0], Vel[0,1], Vel[0,2],Vel[0,3], Vel[0,4], Vel[0,5], Vel[0,6]]
    joints = ["right_s0", "right_s1", "right_e0", "right_e1", "right_w0", "right_w1", "right_w2"]
    #Put theta_dot into a dictionary with joint labels
    theta_dot_dict = {}
    angle_iter = 0
    if theta_dot is not None:
    	for arg in joints:
    		theta_dot_dict[arg] = theta_dot[angle_iter]
        	angle_iter += 1
    
    #Velocity command to Baxter:
    limb.set_joint_velocities(theta_dot_dict)
    #print current_pos
    #print quanser_pos




def listener():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('listener', anonymous=True)
    
    #queue_size of 1 makes it so that it only uses the last set of data it received. If there is a larger queue
    #or no queue_size argument then there will be a delay between when HD2 data is published to the topic
    #and when this node reads that data
    rospy.Subscriber('/hd2/robot_state/', HD2State, callback, queue_size = 1)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    listener()


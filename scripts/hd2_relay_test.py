import socket
import struct
import random

def test_send_to_hd2_socket():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    # Create a string of 8 doubles to send over the socket.
    values = (random.random(), 0.1, 0.2, 0.5, 0.6, 0.7, 0.8, 0.0)
    packer = struct.Struct('d d d d d d d d')
    data = packer.pack(*values)
    s.sendto(data,('127.0.0.1',18000))

if __name__ == '__main__':
    test_send_to_hd2_socket()
